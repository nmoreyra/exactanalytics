### Links a los libros cuyo tamaño es mayor a 100Mb

## Basketball on Paper - Dean Oliver
# Versiones en pdf

https://drive.google.com/file/d/16rNqPKIvxlx67zEhjZP2BQqkz81ogeXF/view?usp=sharing

https://drive.google.com/file/d/1d5gIBQj545XyecMV9nDAinxBftdu-4ZJ/view?usp=sharing

El primer link corresponde al libro escaneado y el segundo al .epub reconvertido en .pdf.