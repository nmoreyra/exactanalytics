# Básquet Estadístico
**Exactas UBA** :basketball:

###   Links Útiles:
* [¿Qué es Git?](https://es.wikipedia.org/wiki/Git)
* [Instalar Git](https://docs.gitlab.com/ee/topics/git/how_to_install_git/index.html)
* [Tutorial Git](Git/Git.md)
___
----
## Contenidos del repositorio
* #### Sobre Exact Analytics.
	* [Objetivos](Contenidos/00_Objetivos.md)
	* [Listado de miembros](Contenidos/01_Listado-miembros.md)
	* [Premios del equipo](Contenidos/02_Premios.md)
	* [Videos del equipo](Contenidos/03_Videos.md)
	* [Contacto](Contenidos/04_Contacto.md)

----
* #### Datos
	* [Base de datos](Contenidos/database/README.md) 
	* [Scripts útiles](Contenidos/scripts/README.md)
	* [Fuentes de información](Contenidos/05_Fuentes.md)
	
